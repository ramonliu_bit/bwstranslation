﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace BWSTranslation
{

    class StructSRT
    {
        public string Duration;
        public string Text;
        public string Translator;
        public FontStyle Style;
        public Color Color;
    }

    class ClassSRT
    {
        private List<StructSRT> _context = null;

        public ClassSRT()
        {
            _context = new List<StructSRT>();
        }

        ~ClassSRT()
        {
            Clear();
            _context = null;
        }

        public int Count()
        {
            return _context.Count;
        }

        public void Clear()
        {
            _context.Clear();
        }

        public string Context(int index)
        {
            StructSRT srt = _context[index];
            return srt.Text;
        }


        public void SetTrans(int index, string text)
        {
            if ((index >= 0) && (index < _context.Count))
            {
                StructSRT srt = _context[index];
                srt.Translator = text;
            }
        }

        public StructSRT GetItem(int index)
        {
            if ((index >= 0) && (index < _context.Count))
                return _context[index];
            else
                return null;
        }

        private void Add(string text, string duration)
        {
            if (text.Length > 0)
            {
                //format ref: https://en.wikipedia.org/wiki/SubRip
                FontStyle fs = new FontStyle();
                fs = FontStyle.Regular;
                Color fc = Color.White;
                if (text.IndexOf("<i>") >= 0)
                {
                    fs = FontStyle.Italic;
                    text = text.Replace("<i>", "").Replace("</i>", "");
                }   
                else
                if (text.IndexOf("<b>") >= 0)
                {
                    fs = FontStyle.Bold;
                    text = text.Replace("<b>", "").Replace("</b>", "");
                }   
                else
                if (text.IndexOf("<u>") >= 0)
                {
                    fs = FontStyle.Underline;
                    text = text.Replace("<u>", "").Replace("</u>", "");
                }
                else
                if (text.IndexOf("<font color=") >= 0)
                {
                    //<font color="color name or #code">
                    int i_s = text.IndexOf("<font color=");
                    int i_e = text.IndexOf(">");
                    string sc = text.Substring(i_s, i_e - i_s).Replace("<font color=", "").Replace("\"","");
                    if (sc[0] == '#')
                        fc = Color.FromArgb(Convert.ToInt32(sc.Replace("#",""), 16));
                    else
                        fc = Color.FromName(sc);
                    text = text.Substring(i_e + 1).Replace("</font>", "");
                }

                _context.Add(new StructSRT()
                {
                    Text = text,
                    Duration = duration,
                    Style = fs,
                    Color = fc,
                });
            }
        }

        public void Parser(string context)
        {
            string[] lines = context.Split(new char[] { '\n', '\r' }, StringSplitOptions.None);

            _context.Clear();
            int idx = 0;
            string str_duration = string.Empty;
            string str_text = string.Empty;
            do
            {
                // index
                // duration
                // text

                string line = lines[idx++];
                if (line.Trim().Length <= 0) continue;
                if (int.TryParse(line, out int index))
                {
                    if (str_text.Length > 0)
                    {
                        Add(str_text, str_duration);
                        str_text = string.Empty;
                    }
                    str_duration = string.Empty;
                }   
                else if ((line.IndexOf("-->") > 0) || (line.IndexOf("- >") > 0))
                {
                    if (str_text.Length > 0)
                    {
                        Add(str_text, str_duration);
                        str_text = string.Empty;
                    }
                    str_duration = line;
                }   
                else
                    str_text += (str_text.Length > 0) ? Environment.NewLine + line : line;

            } while (idx < lines.Length);

        }

        public void SaveToFile(string filename, bool inc_org = false)
        {
            List<string> lst = new List<string>();

            int sequence = 1;
            foreach (StructSRT item in _context)
            {
                lst.Add((sequence++).ToString());
                lst.Add(item.Duration);
                string text = string.Empty;
                if (item.Style != FontStyle.Regular)
                {
                    if (item.Style == FontStyle.Bold)
                        text = "<b>";
                    else if (item.Style == FontStyle.Italic)
                        text = "<i>";
                    else if (item.Style == FontStyle.Underline)
                        text = "<u>";
                }

                if (item.Color != Color.White)
                {
                    text += "<font color=\"#"+ item.Color.ToArgb().ToString("X") +"\">";
                }

                text += item.Translator;
                if (inc_org)
                    text += Environment.NewLine + item.Text;

                if (item.Color != Color.White)
                {
                    text += "</font>";
                }

                if (item.Style != FontStyle.Regular)
                {
                    if (item.Style == FontStyle.Bold)
                        text += "</b>";
                    else if (item.Style == FontStyle.Italic)
                        text += "</i>";
                    else if (item.Style == FontStyle.Underline)
                        text += "</u>";
                }

                lst.Add(text);
                lst.Add("");
            }

            File.WriteAllLines(filename, lst.ToArray());
        }
    }
}
