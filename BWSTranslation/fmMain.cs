﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using CsQuery;
using System.Net;
using System.Net.Http;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Drawing;

namespace BWSTranslation
{
    public partial class fmMain : Form
    {
        private List<string> lstFiles = null;
        private bool _running = false;
        private int MAX_LENGHT = 1024;

        public fmMain()
        {
            InitializeComponent();
        }

        private void fmMain_Shown(object sender, EventArgs e)
        {
#if !DEBUG
            textBox1.Visible = false;
            ClientSize = new Size(Width, btnFiles.Height + btnFiles.Top + 30);
#endif
            toolStripStatusLabel1.Text = "";
            toolStripProgressBar1.Maximum = 100;
            toolStripProgressBar1.Value = 0;
            lstFiles = new List<string>();
            cmbLang.SelectedIndex = 0;
            _running = false;
        }

        private void fmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            _running = false;
            lstFiles.Clear();
            lstFiles = null;
        }

        private void btnFiles_Click(object sender, EventArgs e)
        {
            _running = !_running;
            if (openFileDialog1.InitialDirectory.Length <= 0)
            {
                openFileDialog1.InitialDirectory = Application.StartupPath;
            }
            if ((_running) && (openFileDialog1.ShowDialog() == DialogResult.OK))
            {
                openFileDialog1.InitialDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                lstFiles.Clear();
                lstFiles = openFileDialog1.FileNames.ToList();
                Running();
            }
        }

        private void Running()
        {
            btnFiles.Text = "Stop";
            ProcessTranslationByFiles(lstFiles);
            _running = false;
            btnFiles.Text = "Load";
        }

        private void ProcessTranslationByFiles(List<string> lst)
        {
            int count = 0;
            foreach (string filename in lst)
            {
                if (_running)
                {
                    Hint(String.Format("{0}/{1}:{2}", count++, lst.Count, Path.GetFileNameWithoutExtension(filename)));
                    ProcessTranslation(filename);
                }
                else
                    break;
                Application.DoEvents();
            }
        }

        private void ProcessTranslationByFolder(string folder)
        {
            string ext = "srt";
            int count = 0;
            DirectoryInfo directoryInfo;
            FileInfo[] files;
            directoryInfo = new DirectoryInfo(folder);
            files = directoryInfo.GetFiles("*." + ext, SearchOption.TopDirectoryOnly);
            if (files.Length > 0)
            {
                foreach (FileInfo fi in files)
                {
                    if (_running)
                    {
                        Hint(String.Format("{0}/{1}:{2}", count++, files.Length, Path.GetFileNameWithoutExtension(fi.FullName)));
                        ProcessTranslation(fi.FullName);
                    }
                    else
                        break;
                    Application.DoEvents();
                }
            }
        }

        private void ProcessTranslation(string filename)
        {
            string ext = Path.GetExtension(filename);
            if (ext.ToLower().CompareTo(".srt") == 0)
            {
                TransForSRT(filename);
            }
        }

        private void Hint(string text)
        {
            toolStripStatusLabel1.Text = text;
        }

        private ClassSRT LoadSRTFile(string filename)
        {
            string context = File.ReadAllText(filename);
            ClassSRT csrt = new ClassSRT();
            csrt.Parser(context);
            return csrt;
        }

        private void TransForSRT(string filename)
        {
            Hint("Translation...");
            ClassSRT csrt = LoadSRTFile(filename);
            toolStripProgressBar1.Maximum = csrt.Count();
            toolStripProgressBar1.Value = 0;
            int idx = 0;
            int idx_start = 0;
            string query_text = string.Empty;

            do
            {
                if (!_running) break;
                
                StructSRT st = csrt.GetItem(idx++);
                query_text += st.Text + Environment.NewLine + Environment.NewLine;
                if (query_text.Length > MAX_LENGHT)
                {
                    string trnas = TranslatorQueryText(query_text);
                    textBox1.Text += trnas;
                    SetTranslatorText(csrt, idx_start, idx, trnas);
                    idx_start = idx;
                    query_text = string.Empty;
                }
                toolStripProgressBar1.Value = idx;
                Application.DoEvents();
            } while (idx < csrt.Count());

            if (query_text.Length > 0)
            {
                string trnas = TranslatorQueryText(query_text);
                textBox1.Text += trnas;
                SetTranslatorText(csrt, idx_start, idx, trnas);
                query_text = string.Empty;
            }
            if (_running)
            {
                Hint("Saving...");
                csrt.SaveToFile(filename + ".srt", chkbIncOrg.Checked);
                Hint("Done.");
            }
        }

        private void SetTranslatorText(ClassSRT csrt, int start, int end, string trans)
        {
            string[] lines = trans.Split(new string[] { Environment.NewLine + Environment.NewLine }, StringSplitOptions.None);

            int idx = 0;
            for (int i = start; i < end; i++)
            {
                csrt.SetTrans(i, lines[idx++]);
            }
        }

        private string TranslatorQueryText(string qtext)
        {
            string ret = string.Empty;
            Hint("Query:" + qtext.Substring(0, 8).Trim());

            string enc = HttpUtility.UrlEncode(qtext);

            string url = "http://translate.google.com.tw/" +
                 "translate_t?langpair=auto|" + cmbLang.Text + "&text=" + enc;

            //string url = "https://translate.google.com.tw/?hl=zh-TW&tab=wT#auto/zh-TW/";
            //url += enc;

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.Headers.Add("User-Agent", "Mozilla/5.0 BWS.Translator 1.0");
                client.Headers.Add("Accept", "*/*");
                client.Headers.Add("Accept-Charset", "UTF-8");
            
                CQ html = client.DownloadString(url);
                var result_box = html["#result_box"];
                if (result_box != null)
                {
                    ret = result_box.Text().Replace("\n", Environment.NewLine);
                }
            }

            return ret;
        }

    }
}
